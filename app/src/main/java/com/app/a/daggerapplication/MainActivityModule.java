package com.app.a.daggerapplication;

import android.content.Context;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by sarath on 4/11/17.
 */
@Module(includes = MainActivityModule.MainFragmentProvider.class)
abstract class MainActivityModule {
    @Binds
    @ActivityContext
    abstract Context activityContet(MainActivity mainActivity);



    @Module
    public interface MainFragmentProvider {
        @FragmentScoped
        @ContributesAndroidInjector(modules = MainFragmentModule.class)
        MainFragment bindMainActivity();
    }


}
