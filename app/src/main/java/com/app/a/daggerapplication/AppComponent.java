package com.app.a.daggerapplication;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;

import dagger.BindsInstance;
import dagger.Component;
import dagger.Subcomponent;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

/**
 * Created by sarath on 4/11/17.
 */
@ApplicationScope
@Component(modules = {
        AndroidInjectionModule.class,
        ActivityBuilder.class,
        AppModule.class,

})
public interface AppComponent {

    void inject(App app);


    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
