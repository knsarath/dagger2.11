package com.app.a.daggerapplication;

import javax.inject.Scope;

/**
 * Created by sarath on 4/11/17.
 */
@Scope
public @interface ApplicationScope {
}
