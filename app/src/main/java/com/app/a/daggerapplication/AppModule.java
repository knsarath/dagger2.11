package com.app.a.daggerapplication;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by sarath on 4/11/17.
 */
@Module(includes = {AppModule.Declaration.class})
public class AppModule {
    @Module
    public interface Declaration {
        @Binds
        @ApplicationContext
        Context mContext(Application application);
    }

    @Provides
    @ApplicationScope
    public Gson provideGson() {
        return new Gson();
    }
}
