package com.app.a.daggerapplication;

import javax.inject.Qualifier;

/**
 * Created by sarath on 4/11/17.
 */
@Qualifier
public @interface ApplicationContext {
}
