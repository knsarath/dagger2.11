package com.app.a.daggerapplication;

import com.google.gson.Gson;

import javax.inject.Inject;

/**
 * Created by sarath on 4/11/17.
 */

public class MyGsonClass {
    Gson mGson;

    @Inject
    public MyGsonClass(Gson gson) {
        mGson = gson;
    }

    public Gson getGson() {
        return mGson;
    }
}
